package com.featzima.sqlite;

import android.database.sqlite.SQLiteDatabase;

public interface Migration {

    int getTargetVersion();

    void execute(SQLiteDatabase database);

}
