package com.featzima.sqlite;

import java.util.List;

public interface TodosRepository {

    void insert(Todo todo);

    List<Todo> select();

}
