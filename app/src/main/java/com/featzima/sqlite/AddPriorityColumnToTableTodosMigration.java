package com.featzima.sqlite;

import android.database.sqlite.SQLiteDatabase;

public class AddPriorityColumnToTableTodosMigration implements Migration {
    @Override
    public int getTargetVersion() {
        return 2;
    }

    @Override
    public void execute(SQLiteDatabase database) {
        database.execSQL("ALTER TABLE todos ADD COLUMN priority INTEGER DEFAULT 2");
    }
}
