package com.featzima.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class TodosRepositoryImpl implements TodosRepository {

    final Context context;

    public TodosRepositoryImpl(Context context) {
        this.context = context;
    }

    @Override
    public void insert(Todo todo) {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

//                database.execSQL("INSERT INTO todos (id, todo) VALUES (1, 'Create homework')");

        ContentValues values = new ContentValues();
        values.put(TodoEntity.COLUMN_TODO, todo.getValue());
        database.insert(TodoEntity.TABLE_TODO, "", values);

    }

    @Override
    public List<Todo> select() {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = database.query(TodoEntity.TABLE_TODO, null, null, null, null, null, TodoEntity.COLUMN_ID + " DESC");
        List<Todo> todos = new ArrayList<>();
        while (cursor.moveToNext()) {
            int idColumnIndex = cursor.getColumnIndex(TodoEntity.COLUMN_ID);
            int todoColumnIndex = cursor.getColumnIndex(TodoEntity.COLUMN_TODO);
            int id = cursor.getInt(idColumnIndex);
            String todo = cursor.getString(todoColumnIndex);
            todos.add(new Todo(todo));
        }
        cursor.close();
        return todos;
    }
}
