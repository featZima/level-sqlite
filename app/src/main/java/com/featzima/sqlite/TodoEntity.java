package com.featzima.sqlite;

public class TodoEntity {
    static final String TABLE_TODO = "todos";
    static final String COLUMN_ID = "id";
    static final String COLUMN_TODO = "todo";
    static final String COLUMN_PRIORITY = "priority";
}
