package com.featzima.sqlite;

import android.database.sqlite.SQLiteDatabase;

public class CreateTodosTableMigration implements Migration {

    @Override
    public int getTargetVersion() {
        return 1;
    }

    @Override
    public void execute(SQLiteDatabase database) {
        database.execSQL("CREATE TABLE todos (id INTEGER PRIMARY KEY, todo TEXT)");
    }
}
