package com.featzima.sqlite;

public class Todo {

    final String value;

    public Todo(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
